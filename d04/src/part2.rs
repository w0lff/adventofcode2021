use std::{
    fmt::Display,
    fs::File,
    io::{BufRead, BufReader},
    path::PathBuf,
};

pub fn run(input: PathBuf) {
    println!("{:#?}", input);
    let file = File::open(input).unwrap();
    let mut reader = BufReader::new(&file);
    let mut moves_buf = String::new();
    let mut moves: Vec<u32> = Vec::new();
    let mut boards: BoardVec = BoardVec(Vec::new());

    reader.read_line(&mut moves_buf).unwrap();

    moves = moves_buf
        .trim()
        .split(',')
        .map(|x| x.parse::<u32>().unwrap())
        .collect();

    println!("moves: {:?}", moves);

    reader
        .lines()
        .map(|x| x.unwrap())
        .collect::<Vec<String>>()
        .as_slice()
        .chunks(6)
        .for_each(|lines| {
            let mut board = [[0u32; 5]; 5];
            lines[1..=5].iter().enumerate().for_each(|(i, line)| {
                line.split_whitespace()
                    .map(|x| x.parse::<u32>().unwrap())
                    .enumerate()
                    .for_each(|(j, x)| board[i][j] = x);
            });
            boards.0.push(BingoBoard::from(board));
        });

    println!("{}", boards.0.len());

    let mut winners = Vec::new();
    for n in moves.iter_mut() {
        boards.mark_boards(*n);
        if let Some(mut winner_vec) = boards.has_winners(*n) {
            winners.append(&mut winner_vec);

            println!("another winner at move: {}", n);
        }
    }
    let (last_winner, n) = winners.last().unwrap();
    //let n = &48;
    println!("{}", last_winner);
    println!("last winner move: {}", n);
    println!("last winner board: {}", last_winner);
    println!("score: {}", last_winner.calculate_score(*n));
    println!("{:?}", last_winner.marked);
    println!("{}", boards.0.len())
}

#[derive(Debug, Clone, Copy)]
struct BingoBoard {
    board: [[u32; 5]; 5],
    marked: [[bool; 5]; 5],
}

impl BingoBoard {
    fn mark(&mut self, n: u32) {
        self.board.iter().enumerate().for_each(|(i, l)| {
            l.iter().enumerate().for_each(|(j, x)| {
                if *x == n {
                    self.marked[i][j] = true;
                }
            });
        });
    }

    fn is_winner(&self) -> bool {
        let a = self.marked.iter().any(|l| l.iter().all(|&x| x));
        let mut rows = [false; 5];
        self.marked.iter().flatten().enumerate().for_each(|(i, x)| {
            if !rows[i % 5] {
                rows[i % 5] = rows[i % 5] || *x;
            } else {
                rows[i % 5] = rows[i % 5] && *x;
            }
        });
        let out = a || rows.iter().any(|&x| x);
        // println!("{}", out);
        out
    }

    fn calculate_score(&self, n: u32) -> u32 {
        let mut sum = 0u32;
        self.board.iter().enumerate().for_each(|(i, l)| {
            l.iter().enumerate().for_each(|(j, x)| {
                if !self.marked[i][j] {
                    sum += x;
                }
            });
        });

        sum * n
    }
}

#[derive(Debug, Clone)]
struct BoardVec(Vec<BingoBoard>);

impl BoardVec {
    fn mark_boards(&mut self, n: u32) {
        self.0.iter_mut().for_each(|b| b.mark(n));
    }
    fn has_winner(&self) -> Option<(usize, BingoBoard)> {
        for (i, board) in self.0.iter().enumerate() {
            if board.is_winner() {
                return Some((i, *board));
            }
        }
        None
    }
    fn has_winners(&mut self, n: u32) -> Option<Vec<(BingoBoard, u32)>> {
        let mut vec = Vec::new();
        while let Some(winner) = self.has_winner() {
            vec.push((winner.1, n));
            self.0.remove(winner.0);
        }

        if vec.len() > 0 {
            Some(vec)
        } else {
            None
        }
    }
}

impl From<[[u32; 5]; 5]> for BingoBoard {
    fn from(b: [[u32; 5]; 5]) -> Self {
        Self {
            board: b,
            marked: [[false; 5]; 5],
        }
    }
}

impl Display for BingoBoard {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        for line in self.board.iter() {
            writeln!(f)?;
            for (j, x) in line.iter().enumerate() {
                if j == 0 {
                    write!(f, "{:2}", x)?
                } else {
                    write!(f, "{:3}", x)?
                }
            }
        }
        Ok(())
    }
}

impl Display for BoardVec {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        for board in self.0.iter() {
            writeln!(f, "{}", board)?
        }
        Ok(())
    }
}
