use std::{
    fmt::Display,
    fs::File,
    io::{BufRead, BufReader},
    path::PathBuf,
};

pub fn run(input: PathBuf) {
    println!("{:#?}", input);
    let file = File::open(input).unwrap();
    let mut reader = BufReader::new(&file);
    let mut moves_buf = String::new();
    let mut moves: Vec<u32> = Vec::new();
    let mut boards: BoardVec = BoardVec(Vec::new());

    reader.read_line(&mut moves_buf).unwrap();

    moves = moves_buf
        .trim()
        .split(',')
        .map(|x| x.parse::<u32>().unwrap())
        .collect();

    println!("moves: {:?}", moves);

    reader
        .lines()
        .map(|x| x.unwrap())
        .collect::<Vec<String>>()
        .as_slice()
        .chunks(6)
        .for_each(|lines| {
            let mut board = [[0u32; 5]; 5];
            lines[1..=5].iter().enumerate().for_each(|(i, line)| {
                line.split_whitespace()
                    .map(|x| x.parse::<u32>().unwrap())
                    .enumerate()
                    .for_each(|(j, x)| board[i][j] = x);
            });
            boards.0.push(BingoBoard::from(board));
        });

    // println!("{}", boards);

    for n in moves.iter_mut() {
        boards.mark_boards(*n);
        if let Some(board) = boards.has_winner() {
            println!("winner move: {}", *n);
            println!("winner board: {}", board);
            println!("score: {}", board.calculate_score(*n));
            break;
        }
    }
}

#[derive(Debug, Clone, Copy)]
struct BingoBoard {
    board: [[u32; 5]; 5],
    marked: [[bool; 5]; 5],
}

impl BingoBoard {
    fn mark(&mut self, n: u32) {
        self.board.iter().enumerate().for_each(|(i, l)| {
            l.iter().enumerate().for_each(|(j, x)| {
                if *x == n {
                    self.marked[i][j] = true;
                }
            });
        });
    }

    fn is_winner(&self) -> bool {
        let a = self.marked.iter().any(|l| l.iter().all(|&x| x));
        if a {
            return a;
        }
        let mut rows = [true; 5];
        self.marked.iter().flatten().enumerate().for_each(|(i, x)| {
            rows[i % 5] &= x;
        });
        rows.iter().any(|&x| x)
    }

    fn calculate_score(&self, n: u32) -> u32 {
        let mut sum = 0u32;
        self.board.iter().enumerate().for_each(|(i, l)| {
            l.iter().enumerate().for_each(|(j, x)| {
                if !self.marked[i][j] {
                    sum += x;
                }
            });
        });

        sum * n
    }
}

#[derive(Debug, Clone)]
struct BoardVec(Vec<BingoBoard>);

impl BoardVec {
    fn mark_boards(&mut self, n: u32) {
        self.0.iter_mut().for_each(|b| b.mark(n));
    }
    fn has_winner(&self) -> Option<BingoBoard> {
        for board in self.0.iter() {
            if board.is_winner() {
                return Some(*board);
            }
        }
        None
    }
}

enum Result<T, E> {
    Ok(T),
    Err(E),
}

impl From<[[u32; 5]; 5]> for BingoBoard {
    fn from(b: [[u32; 5]; 5]) -> Self {
        Self {
            board: b,
            marked: [[false; 5]; 5],
        }
    }
}

impl Display for BingoBoard {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        for line in self.board.iter() {
            writeln!(f)?;
            for (j, x) in line.iter().enumerate() {
                if j == 0 {
                    write!(f, "{:2}", x)?
                } else {
                    write!(f, "{:3}", x)?
                }
            }
        }
        Ok(())
    }
}

impl Display for BoardVec {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        for board in self.0.iter() {
            writeln!(f, "{}", board)?
        }
        Ok(())
    }
}
