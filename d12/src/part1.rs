use std::{
    fs::File,
    io::{BufRead, BufReader},
    path::PathBuf,
};

pub fn run(input: PathBuf) {
    env_logger::init();
    println!("{:#?}", input);
    let file = File::open(input).unwrap();
    let reader = BufReader::new(&file);
    let mut graph = Graph::default();

    reader.lines().map(|l| l.unwrap()).for_each(|line| {
        graph.parse_line(line);
    });

    log::debug!("{:#?}", graph);
}
#[derive(Debug, Clone, PartialEq)]
struct Node {
    node_type: NodeType,
    visit_count: u32,
}

impl Node {
    fn new(node_type: NodeType) -> Self {
        Self {
            node_type,
            visit_count: 0,
        }
    }
}

#[derive(Debug, Clone, PartialEq)]
enum NodeType {
    Start,
    End,
    Small(String),
    Big(String),
}

#[derive(Debug, Default, Clone)]
struct Graph {
    vertices: Vec<Node>,
    edges: Vec<(Node, Node)>,
}

impl Graph {
    fn parse_line(&mut self, line: String) {
        let nodes: Vec<Node> = line
            .split('-')
            .map(|e| -> Node {
                match e {
                    "start" => Node::new(NodeType::Start),
                    "end" => Node::new(NodeType::End),
                    e if (e.to_uppercase() == e) => Node::new(NodeType::Big(e.to_owned())),
                    e if (e.to_lowercase() == e) => Node::new(NodeType::Small(e.to_owned())),
                    _ => {
                        panic!("Error in parsing")
                    }
                }
            })
            .collect();

        nodes.iter().for_each(|n| {
            if self.vertices.iter().all(|e| e != n) {
                self.vertices.push(n.clone())
            }
        });
        let edge = (
            nodes.first().unwrap().clone(),
            nodes.last().unwrap().clone(),
        );
        self.edges.push(edge);
    }
}

type Path = Vec<Node>;

fn bfs(graph: Graph) -> Vec<Path> {
    let unvisited = graph.vertices.clone();
    let paths = Vec::new();
    let graph = graph;

    return paths;
}
