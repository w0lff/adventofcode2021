fmt:
	cargo fmt

clean:
	cargo clean

run PACKAGE PART='1' +INPUT='input.txt': fmt
	cargo run -p {{PACKAGE}} -- {{PART}} {{PACKAGE}}/{{INPUT}}

runex PACKAGE='' PART='1' INPUT='input-example.txt': fmt
	cargo run -p {{PACKAGE}} -- {{PART}} {{PACKAGE}}/{{INPUT}}

runall part='1': fmt
	for day in `ls -d d*`; do \
		cargo run -p $day -- {{part}} $day/input.txt; \
	done

test PACKAGE: fmt
	cargo test -p {{PACKAGE}}

