use std::{
    fmt::Display,
    fs::File,
    io::{BufRead, BufReader},
    ops::Index,
    path::PathBuf,
};

pub fn run(input: PathBuf) {
    env_logger::builder().parse_filters("trace").init();
    println!("{:#?}", input);
    let file = File::open(input).unwrap();
    let mut reader = BufReader::new(&file);
    let mut start = String::new();

    reader.read_line(&mut start).unwrap();
    let start = start.trim().as_bytes().to_vec();

    log::info!("Input Chain: {:?}", start);

    let mut pol = Polymer {
        chain: start,
        operations: Vec::new(),
    };

    reader.lines().map(|l| l.unwrap()).for_each(|line| {
        if line.is_empty() {
            return;
        }
        log::trace!("{:?}", line);
        pol.add_operation(line);
    });

    log::trace!("Polymer: {:?}", pol);

    for i in 0..2 {
        pol.step();
        log::debug!(
            "Polymer Chain: {:?}",
            String::from_utf8(pol.chain.clone()).unwrap()
        );
    }

    log::info!("{:?}", pol.get_uniques());

    let mut occurences = Vec::new();
    for uni in pol.get_uniques() {
        let c = pol.count(uni);
        occurences.push((String::from_utf8(vec![uni]).unwrap(), c));
    }
    occurences.sort_by(|a, b| a.1.partial_cmp(&b.1).unwrap());
    log::info!("first: {:#?}", occurences.first().unwrap());

    log::info!("last: {:#?}", occurences.last().unwrap());
}

#[derive(Debug, Clone, PartialEq)]
struct Polymer {
    chain: Vec<u8>,
    operations: Vec<PolymerInsert>,
}

#[derive(Debug, Clone, PartialEq)]
struct PolymerInsert {
    matching: String,
    insert: String,
}

impl Polymer {
    fn add_operation(&mut self, line: String) {
        let mut v: Vec<String> = line.split(" -> ").map(|e| e.to_owned()).collect();
        let op = PolymerInsert {
            matching: v[0].clone(),
            insert: format!("{}{}{}", v[0].remove(0), v[1].clone(), v[0].remove(0),),
        };

        self.operations.push(op);
    }

    fn step(&mut self) {
        let mut matched_rules = Vec::new();
        self.chain.windows(2).for_each(|w| {
            self.operations.iter().for_each(|op| {
                if w.eq(op.matching.as_bytes()) && !matched_rules.contains(&op) {
                    matched_rules.push(op);
                }
            });
        });
        log::trace!("matched_rules: {:?}", matched_rules);

        matched_rules
            .iter()
            .for_each(|op| self.chain = op.apply_op(self.chain.clone()));
    }

    fn get_uniques(&self) -> Vec<u8> {
        let mut uniques = Vec::new();
        self.chain.iter().for_each(|&a| {
            if !uniques.contains(&a) {
                uniques.push(a);
            }
        });
        uniques
    }

    fn count(&self, x: u8) -> u32 {
        let mut count = 0u32;
        self.chain.iter().for_each(|c| {
            if *c == x {
                count += 1
            }
        });
        count
    }
}

impl PolymerInsert {
    fn apply_op(&self, chain: Vec<u8>) -> Vec<u8> {
        String::from_utf8(chain)
            .unwrap()
            .replace(&self.matching, &self.insert)
            .into_bytes()
    }
}
