use std::path::PathBuf;

pub fn run(input: PathBuf) {
    println!("{:#?}", input);
}
