use std::fs::File;
use std::io::{BufRead, BufReader};
use std::path::PathBuf;

#[derive(Debug)]
struct Submarine {
    depth: u32,
    xpos: u32,
    aim: u32,
}

#[derive(Debug, Clone, Copy)]
enum SubmarineCommand {
    Forward(u32),
    Up(u32),
    Down(u32),
}

#[derive(Debug, Clone, Copy)]
enum SubmarineError {
    ParseError,
}

impl Submarine {
    fn parse_command(s: &str) -> Result<SubmarineCommand, SubmarineError> {
        let mut s = s.trim().split_whitespace();
        let cmd: &str;
        let num: u32;

        cmd = s.next().ok_or(SubmarineError::ParseError)?;
        num = s
            .next()
            .ok_or(SubmarineError::ParseError)?
            .parse::<u32>()
            .or(Err(SubmarineError::ParseError))?;

        match cmd {
            "forward" => Ok(SubmarineCommand::Forward(num)),
            "down" => Ok(SubmarineCommand::Down(num)),
            "up" => Ok(SubmarineCommand::Up(num)),
            _ => Err(SubmarineError::ParseError),
        }
    }
    fn exec_command(&mut self, cmd: SubmarineCommand) {
        match cmd {
            SubmarineCommand::Forward(n) => {
                self.xpos += n;
                self.depth += self.aim * n;
            }
            SubmarineCommand::Down(n) => self.aim += n,
            SubmarineCommand::Up(n) => self.aim -= n,
        }
    }

    fn mul(&self) -> u32 {
        self.depth * self.xpos
    }
}

impl Default for Submarine {
    fn default() -> Self {
        Self {
            depth: Default::default(),
            xpos: Default::default(),
            aim: Default::default(),
        }
    }
}

pub(crate) fn run(input: PathBuf) {
    println!("{:#?}", input);
    let file = File::open(input).unwrap();
    let reader = BufReader::new(file);
    let mut sub = Submarine::default();

    for line in reader.lines() {
        let line = line.unwrap();
        let cmd_res = Submarine::parse_command(&line);
        // println!("{:?}", cmd_res);
        let cmd = cmd_res.unwrap();

        sub.exec_command(cmd);
    }
    println!("{:#?}", &sub);
    println!("{:#?}", &sub.mul());
}
