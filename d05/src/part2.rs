use std::{
    fmt::Display,
    fs::File,
    io::{BufRead, BufReader},
    path::PathBuf,
};

pub fn run(input: PathBuf) {
    println!("{:#?}", input);
    let file = File::open(input).unwrap();
    let mut reader = BufReader::new(&file);
    let mut ocean_floor = OceanFloor::default();

    reader.lines().map(|x| x.unwrap()).for_each(|line| {
        let vline = OceanFloor::parse_line(line);
        ocean_floor.add_lines(vline)
    });

    println!("overlapping lines: {}", ocean_floor.count_overlap());
}

#[derive(Debug, Clone, Default)]
struct OceanFloor {
    vents: Vec<Vec<u32>>,
}

#[derive(Debug, Clone, Copy, Default)]
struct VentLine {
    x1: usize,
    y1: usize,
    x2: usize,
    y2: usize,
}

impl VentLine {
    fn is_line(&self) -> bool {
        // [0,y1] -> [0,y2]
        // vertical
        self.x1 == self.x2 ||
        // [x1,0] -> [x2,0]
        // horizontal
            self.y1 == self.y2
    }

    fn is_diagonal(&self) -> bool {
        // [0,0] -> [2,2]
        (self.x1 == self.y1 && self.x2 == self.y2) ||
        // [2,0] -> [0,2]
            (self.x1 == self.y2 && self.x2 == self.y1)
    }

    fn line_points(&self) -> Vec<(usize, usize)> {
        let v = Vec::new();

        let x = self.x2 - self.x1;
        let y = self.y2 - self.y1;

        v.push((x, y));
        v
    }
}

impl OceanFloor {
    fn parse_line(line: String) -> VentLine {
        let a: Vec<usize> = line
            .split(" -> ")
            .map(|x| x.split(',').map(|x| x.parse::<usize>().unwrap()))
            .flatten()
            .collect();

        // println!("{:?}", a);

        VentLine {
            x1: a[0],
            y1: a[1],
            x2: a[2],
            y2: a[3],
        }
    }

    fn add_lines(&mut self, vline: VentLine) {
        if vline.is_line() {
            self.add_area(vline)
        }
    }

    fn add_diagonal(&mut self, vline: VentLine) {
        for y in y_min0..=y_max {
            if self.vents.len() <= y_max + 1 {
                let mut a = vec![Vec::new(); (y_max + 1 - self.vents.len())];
                self.vents.append(&mut a);
            }
            for x in x_min..=x_max {
                if self.vents[y].len() <= x_max {
                    let mut b = vec![0u32; (x_max + 1 - self.vents[y].len())];
                    self.vents[y].append(&mut b);
                }
                self.vents[y][x] += 1;
            }
        }
    }

    fn add_area(&mut self, vline: VentLine) {
        let x_min = vline.x1.min(vline.x2);
        let x_max = vline.x1.max(vline.x2);
        let y_min = vline.y1.min(vline.y2);
        let y_max = vline.y1.max(vline.y2);

        for y in y_min..=y_max {
            if self.vents.len() <= y_max + 1 {
                let mut a = vec![Vec::new(); (y_max + 1 - self.vents.len())];
                self.vents.append(&mut a);
            }
            for x in x_min..=x_max {
                if self.vents[y].len() <= x_max {
                    let mut b = vec![0u32; (x_max + 1 - self.vents[y].len())];
                    self.vents[y].append(&mut b);
                }
                self.vents[y][x] += 1;
            }
        }
    }

    fn count_overlap(&self) -> u32 {
        let mut sum = 0;
        for line in self.vents.iter() {
            for x in line.iter() {
                if *x >= 2 {
                    sum += 1;
                }
            }
        }
        sum
    }
}

impl Display for OceanFloor {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        for line in self.vents.iter() {
            writeln!(f)?;
            for (j, x) in line.iter().enumerate() {
                write!(f, "{:1x}", x)?
            }
        }
        Ok(())
    }
}
