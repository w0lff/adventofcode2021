use std::{
    fmt::Display,
    fs::File,
    io::{BufRead, BufReader},
    path::PathBuf,
};

pub fn run(input: PathBuf) {
    println!("{:#?}", input);
    let file = File::open(input).unwrap();
    let mut reader = BufReader::new(&file);
    let mut ocean_floor = OceanFloor::default();

    reader.lines().map(|x| x.unwrap()).for_each(|line| {
        let (from, to) = OceanFloor::parse_line(line);
        ocean_floor.add_lines(from, to)
    });

    println!("overlapping lines: {}", ocean_floor.count_overlap());
}

#[derive(Debug, Clone, Default)]
struct OceanFloor {
    vents: Vec<Vec<u32>>,
}

impl OceanFloor {
    fn parse_line(line: String) -> ((usize, usize), (usize, usize)) {
        let a: Vec<usize> = line
            .split(" -> ")
            .map(|x| x.split(',').map(|x| x.parse::<usize>().unwrap()))
            .flatten()
            .collect();

        // println!("{:?}", a);

        let from = (a[0], a[1]);
        let to = (a[2], a[3]);

        (from, to)
    }

    fn add_lines(&mut self, from: (usize, usize), to: (usize, usize)) {
        if from.0 == to.0 || from.1 == to.1 {
            self.add_area(from, to)
        }
    }

    fn add_area(&mut self, from: (usize, usize), to: (usize, usize)) {
        let x_min = from.0.min(to.0);
        let x_max = from.0.max(to.0);
        let y_min = from.1.min(to.1);
        let y_max = from.1.max(to.1);

        for y in y_min..=y_max {
            if self.vents.len() <= y_max + 1 {
                let mut a = vec![Vec::new(); (y_max + 1 - self.vents.len())];
                self.vents.append(&mut a);
            }
            for x in x_min..=x_max {
                if self.vents[y].len() <= x_max {
                    let mut b = vec![0u32; (x_max + 1 - self.vents[y].len())];
                    self.vents[y].append(&mut b);
                }
                self.vents[y][x] += 1;
            }
        }
    }

    fn count_overlap(&self) -> u32 {
        let mut sum = 0;
        for line in self.vents.iter() {
            for x in line.iter() {
                if *x >= 2 {
                    sum += 1;
                }
            }
        }
        sum
    }
}

impl Display for OceanFloor {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        for line in self.vents.iter() {
            writeln!(f)?;
            for (j, x) in line.iter().enumerate() {
                write!(f, "{:1x}", x)?
            }
        }
        Ok(())
    }
}
