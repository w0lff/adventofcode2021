use std::{
    collections::HashMap,
    fs::File,
    hash::{Hash, Hasher},
    io::{BufRead, BufReader},
    path::PathBuf,
};

use itertools::Itertools;

#[inline(never)]
pub fn run(input: PathBuf) {
    env_logger::builder().parse_default_env().init();
    log::info!("Input: {:?}", input);
    let file = File::open(input).unwrap();
    let reader = BufReader::new(&file);

    let lines: Vec<String> = reader.lines().map(|l| l.unwrap()).collect();
    let graph = Graph::from_lines(lines);
    let start = graph.vertices[0];

    let mut d = Dijkstra::new(graph, start);

    log::info!("Starting Dijkstra");
    d.dijkstra();
    log::info!("Finished Dijkstra");
    let end_node = d.get_end_node();
    log::info!("end node risk: {:?}", end_node.distance);
}

#[derive(Debug, Clone, Copy, Hash, PartialEq, Eq)]
struct Vertex {
    risk: u8,
    x: usize,
    y: usize,
}

#[derive(Debug, Clone, Copy)]
struct Edge {
    v1: Vertex,
    v2: Vertex,
}

#[derive(Debug, Default, Clone)]
struct Graph {
    vertices: Vec<Vertex>,
    edges: Vec<Edge>,
    v_index: HashMap<(usize, usize), Vertex>,
    max_x: usize,
    max_y: usize,
}

#[derive(Debug, Clone, Copy)]
struct DNode {
    vertex: Vertex,
    distance: u32,
    visited: bool,
    predecessor: Option<Vertex>,
}

#[derive(Debug, Default, Clone)]
struct DPath {
    path: Vec<Edge>,
}

#[derive(Debug, Clone)]
struct Dijkstra {
    graph: Graph,
    start_node: DNode,
    nodes: Vec<DNode>,
}

impl Vertex {
    fn new(risk: u8, x: usize, y: usize) -> Self {
        Self { risk, x, y }
    }
    #[inline(never)]
    fn is_adjacent(&self, v: &Vertex) -> bool {
        (self.x == v.x && usize_adjacent(self.y, v.y))
            || (usize_adjacent(self.x, v.x) && self.y == v.y)
    }
}

fn usize_adjacent(a: usize, b: usize) -> bool {
    a + 1 == b || b + 1 == a
}

impl Edge {
    fn new(v1: Vertex, v2: Vertex) -> Self {
        Self { v1, v2 }
    }

    fn contains_vertex(&self, v: Vertex) -> bool {
        self.v1 == v || self.v2 == v
    }
}
impl Graph {
    fn from_lines(lines: Vec<String>) -> Self {
        let x = lines[0].len();
        let y = lines.len();
        log::trace!("y: {}", y);
        log::trace!("x: {}", x);

        let mut graph = Graph::with_capacitiy(x, y);
        lines.iter().enumerate().for_each(|(y, l)| {
            l.as_bytes().iter().enumerate().for_each(|(x, n)| {
                let risk: u8 = String::from_utf8(vec![*n]).unwrap().parse().unwrap();
                let vertex = Vertex::new(risk, x, y);
                graph.vertices.push(vertex);
            })
        });
        log::debug!("Graph Vertices Created");
        graph.create_edges();

        graph
    }

    fn with_capacitiy(x: usize, y: usize) -> Self {
        // let num_of_edges = ((x - 1) * (y - 1) * 4 / 2) + (x + y + x - 1 + y - 2);
        let num_of_edges = (x - 1) * y + (y - 1) * x;
        log::trace!("num of edges: {}", num_of_edges);

        Self {
            vertices: Vec::with_capacity(x * y),
            edges: Vec::with_capacity(num_of_edges),
            v_index: HashMap::with_capacity(x * y),
            max_x: x - 1,
            max_y: y - 1,
        }
    }

    fn build_index(&mut self) {
        let index = {
            let mut i = HashMap::new();

            i.try_reserve(self.vertices.len()).unwrap();

            self.vertices.iter().for_each(|v| {
                let k = (v.x, v.y);
                i.insert(k, *v);
            });
            i
        };

        self.v_index = index;
        log::debug!("Index generated");
    }

    #[inline(never)]
    fn create_edges(&mut self) {
        self.build_index();
        log::debug!("v_index len: {:?}", self.v_index.len());
        self.vertices.iter().for_each(|v1| {
            let neighbors = get_neighbors(v1.x, v1.y, self.max_x, self.max_y);
            for k in neighbors.iter() {
                log::trace!("k: {:?}", k);
                let v2 = self.v_index.get(k).unwrap();
                let edge = Edge::new(*v1, *v2);
                self.edges.push(edge);
            }
        });

        self.edges = self.edges.iter().unique().cloned().collect_vec();
        log::debug!("Graph Edges Created");
    }

    fn get_edges_of_vertex(&self, v: Vertex) -> Vec<Edge> {
        let mut edges = Vec::new();
        self.edges.iter().for_each(|e| {
            if e.contains_vertex(v) {
                edges.push(*e);
            }
        });
        edges
    }

    fn get_adjacent_vertices(&self, v: Vertex) -> Vec<Vertex> {
        get_neighbors(v.x, v.y, self.max_x, self.max_y)
            .iter()
            .map(|k| *self.v_index.get(k).unwrap())
            .collect()
    }
}

fn get_neighbors(x: usize, y: usize, max_x: usize, max_y: usize) -> Vec<(usize, usize)> {
    let mut neighbors = Vec::new();
    neighbors.try_reserve(4).unwrap();
    if x != 0 {
        neighbors.push((x - 1, y));
    }
    if y != 0 {
        neighbors.push((x, y - 1));
    }
    if x != max_x {
        neighbors.push((x + 1, y));
    }
    if y != max_y {
        neighbors.push((x, y + 1));
    }
    neighbors
}

impl DNode {
    fn calculate_distance(&self, distance: u32) -> u32 {
        self.vertex.risk as u32 + distance
    }

    fn update(&mut self, predecessor: Vertex, distance: u32) {
        self.predecessor = Some(predecessor);
        self.distance = distance;
    }
}

impl DPath {
    fn calculate_path_risk(&self) -> u32 {
        let mut path_risk = 0;

        self.path.iter().for_each(|e| path_risk += e.v2.risk as u32);
        path_risk
    }
}

impl Dijkstra {
    fn new(graph: Graph, start: Vertex) -> Self {
        log::trace!("Dijkstra start: {:?}", start);
        let start_node = DNode {
            vertex: start,
            distance: 0,
            visited: false,
            predecessor: None,
        };

        let nodes = graph
            .vertices
            .iter()
            .map(|v| {
                if *v != start {
                    let n = DNode {
                        vertex: *v,
                        distance: u32::MAX,
                        visited: false,
                        predecessor: None,
                    };
                    n
                } else {
                    start_node
                }
            })
            .collect();

        Dijkstra {
            graph,
            start_node,
            nodes,
        }
    }

    fn dijkstra(&mut self) {
        let mut last_progress_update = std::time::Instant::now();

        while self.unvisited_node_exists() {
            let nodes_len = self.nodes.len();
            for i in 0..nodes_len {
                self.nodes
                    .sort_by(|n1, n2| n1.distance.partial_cmp(&n2.distance).unwrap());
                self.nodes[i].visited = true;
                let node = self.nodes[i];
                log::trace!("node: {:?}", node);

                let neighbors_v = self.graph.get_adjacent_vertices(node.vertex);
                let mut neighbors = self.get_nodes_from_vertices(neighbors_v);

                for current_neighbor in neighbors.iter_mut() {
                    let new_distance = current_neighbor.calculate_distance(node.distance);
                    log::trace!("new_distance: {}", new_distance);
                    if new_distance < current_neighbor.distance {
                        current_neighbor.update(node.vertex, new_distance);
                        self.update_node(*current_neighbor);
                    }
                }

                let now = std::time::Instant::now();
                let diff_time = std::time::Duration::from_millis(10_000);
                if now.duration_since(last_progress_update) > diff_time {
                    let progress = self.count_visited_nodes() * 100 / nodes_len;
                    last_progress_update = now;
                    log::info!("Dijkstra Progress: {}%", progress);
                }
            }
        }
    }

    fn unvisited_node_exists(&self) -> bool {
        self.nodes.iter().any(|n| !n.visited)
    }

    fn count_visited_nodes(&self) -> usize {
        let mut count = 0usize;
        self.nodes.iter().for_each(|n| {
            if n.visited {
                count += 1;
            }
        });
        count
    }

    fn get_nodes_from_vertices(&self, vertices: Vec<Vertex>) -> Vec<DNode> {
        self.nodes
            .iter()
            .filter(|&n| vertices.iter().any(|&v| v == n.vertex))
            .cloned()
            .collect()
    }

    fn update_node(&mut self, node: DNode) {
        self.nodes.iter_mut().for_each(|n| {
            if node.vertex == n.vertex {
                *n = node
            }
        });
    }

    fn get_end_node(&self) -> DNode {
        *self.nodes.last().unwrap()
    }
}

impl PartialEq for Edge {
    fn eq(&self, other: &Self) -> bool {
        self.v1 == other.v1 && self.v2 == other.v2 || self.v1 == other.v2 && self.v2 == other.v1
    }
}
impl Eq for Edge {}

impl Hash for Edge {
    fn hash<S: Hasher>(&self, state: &mut S) {
        if self.v1.x < self.v2.x || (self.v1.x == self.v2.x && self.v1.y < self.v2.y) {
            self.v1.x.hash(state);
            self.v1.y.hash(state);

            self.v2.x.hash(state);
            self.v2.y.hash(state);
        } else {
            self.v2.x.hash(state);
            self.v2.y.hash(state);

            self.v1.x.hash(state);
            self.v1.y.hash(state);
        }
    }
}

// https://github.com/rust-lang/rust/issues/89492
const fn abs_diff(a: usize, b: usize) -> usize {
    if a < b {
        b.wrapping_sub(a)
    } else {
        a.wrapping_sub(b)
    }
}

#[cfg(test)]
mod test {
    use std::collections::HashSet;

    use super::Edge;
    use super::Graph;
    use super::Vertex;
    use rstest::rstest;

    #[rstest]
    #[case(1, 1, 1, 2)]
    #[case(1, 1, 1, 0)]
    #[case(1, 1, 0, 1)]
    #[case(1, 1, 2, 1)]
    fn vertex_adjacency(
        #[case] x1: usize,
        #[case] y1: usize,
        #[case] x2: usize,
        #[case] y2: usize,
    ) {
        let v1 = Vertex::new(1, x1, y1);
        let v2 = Vertex::new(1, x2, y2);

        assert!(v1.is_adjacent(&v2));
    }
    #[rstest]
    #[case(1, 1, 0, 0)]
    #[case(1, 1, 2, 0)]
    #[case(1, 1, 1, 1)]
    #[case(1, 1, 2, 0)]
    #[case(1, 1, 2, 2)]
    fn vertex_non_adjacency(
        #[case] x1: usize,
        #[case] y1: usize,
        #[case] x2: usize,
        #[case] y2: usize,
    ) {
        let v1 = Vertex::new(1, x1, y1);
        let v2 = Vertex::new(1, x2, y2);

        assert!(!v1.is_adjacent(&v2));
    }

    #[rstest]
    fn edge_contains_vertex() {
        let v1 = Vertex::new(1, 1, 1);
        let v2 = Vertex::new(1, 1, 2);
        let edge = Edge::new(v1, v2);

        assert!(edge.contains_vertex(v1));
        assert!(edge.contains_vertex(v2));
    }

    #[rstest]
    fn graph_get_vertex_edges() {
        use indoc::indoc;
        let input = indoc! {
            "1163751742
            1381373672
            2136511328
            3694931569
            7463417111
            1319128137
            1359912421
            3125421639
            1293138521
            2311944581"
        }
        .lines()
        .map(|s| s.to_string())
        .collect::<Vec<String>>();

        let graph = Graph::from_lines(input);
        let v11 = Vertex::new(3, 1, 1);

        let v10 = Vertex::new(1, 1, 0);
        let v01 = Vertex::new(1, 0, 1);
        let v21 = Vertex::new(8, 2, 1);
        let v12 = Vertex::new(1, 1, 2);
        let ev = graph.get_edges_of_vertex(v11);

        assert!(ev.len() == 4);

        assert!(ev.iter().any(|e| e.contains_vertex(v10)));
        assert!(ev.iter().any(|e| e.contains_vertex(v01)));
        assert!(ev.iter().any(|e| e.contains_vertex(v21)));
        assert!(ev.iter().any(|e| e.contains_vertex(v12)));
    }

    #[rstest]
    fn edge_dedup() {
        let mut ev = Vec::new();
        let v1 = Vertex::new(1, 5, 5);
        let v2 = Vertex::new(8, 4, 5);
        let v3 = Vertex::new(4, 4, 4);

        let edge = Edge::new(v1, v2);
        ev.push(edge);
        let edge = Edge::new(v1, v3);
        ev.push(edge);
        let edge = Edge::new(v2, v1);
        ev.push(edge);

        ev.dedup();

        assert_eq!(ev.len(), 3);
    }

    #[rstest]
    fn edge_dedup_by_unique() {
        use itertools::Itertools;

        let mut ev = Vec::new();
        let v1 = Vertex::new(1, 5, 5);
        let v2 = Vertex::new(8, 4, 5);
        let v3 = Vertex::new(4, 4, 4);

        let edge = Edge::new(v1, v2);
        ev.push(edge);
        let edge = Edge::new(v1, v3);
        ev.push(edge);
        let edge = Edge::new(v2, v1);
        ev.push(edge);

        ev = ev.into_iter().unique().collect();

        assert_eq!(ev.len(), 2);
    }
}
