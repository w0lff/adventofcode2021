use std::{
    fs::File,
    io::{BufRead, BufReader},
    path::PathBuf,
};

pub fn run(input: PathBuf) {
    let file = File::open(input).unwrap();
    let reader = BufReader::new(&file);

    let lines: Vec<String> = reader.lines().map(|l| l.unwrap()).collect();
    let n_lines = bar(lines);
    let n_lines = foo(n_lines);
    for ele in n_lines.iter() {
        println!("{}", ele);
    }
}

fn foo(mut lines: Vec<String>) -> Vec<String> {
    let len = lines.len();
    for i in 1..5 {
        for index in 0..len {
            let mut append_line = String::with_capacity(lines[index].len());

            lines[index]
                .as_bytes()
                .iter()
                .enumerate()
                .for_each(|(x, n)| {
                    let risk: u8 = String::from_utf8(vec![*n]).unwrap().parse().unwrap();
                    let risk = (risk + i - 1) % 9 + 1;
                    append_line.push_str(&format!("{}", risk));
                });
            lines.push(append_line);
        }
    }
    lines
}

fn bar(mut lines: Vec<String>) -> Vec<String> {
    lines
        .iter_mut()
        .map(|l| {
            let mut append_line = String::with_capacity(l.len() * 5);
            for i in 1..5 {
                l.as_bytes().iter().enumerate().for_each(|(x, n)| {
                    let risk: u8 = String::from_utf8(vec![*n]).unwrap().parse().unwrap();
                    let risk = (risk + i - 1) % 9 + 1;
                    append_line.push_str(&format!("{}", risk));
                });
            }
            l.push_str(&append_line);
            l.clone()
        })
        .collect()
}
