use std::{
    fs::File,
    io::{BufRead, BufReader},
    path::PathBuf,
};

use clap::Parser;

#[derive(Parser)]
struct Opts {
    #[clap(default_value = "1")]
    part: u8,
    #[clap(default_value = "input.txt")]
    input: PathBuf,
}

fn main() {
    let opts = Opts::parse();
    match opts.part {
        1 => part1(opts.input),
        2 => part2(opts.input),
        _ => {
            eprintln!("Not a valid part");
        }
    }
}

fn part1(input: PathBuf) {
    println!("{:#?}", input);
    let file = File::open(input).unwrap();
    let reader = BufReader::new(file);

    let mut prev_depth = 0u32;
    let mut increased_count = 0u32;
    for line in reader.lines() {
        let line = line.unwrap();
        let depth = match line.parse::<u32>() {
            Ok(d) => d,
            Err(err) => {
                eprintln!("{:#?}", err);
                continue;
            }
        };
        if prev_depth == 0 {
            prev_depth = depth;
            continue;
        }
        if depth > prev_depth {
            increased_count += 1;
        }
        prev_depth = depth;
    }
    println!("{}", increased_count);
}

fn part2(input: PathBuf) {
    println!("{:#?}", input);
    let file = File::open(input).unwrap();
    let reader = BufReader::new(file);
    let mut count = 0u32;

    let a: Vec<u32> = reader
        .lines()
        .map(|x| x.unwrap().parse::<u32>().unwrap())
        .collect::<Vec<u32>>()
        .windows(3)
        .map(|w| w.iter().sum())
        .collect::<Vec<u32>>();

    let _b: Vec<bool> = a
        .windows(2)
        .map(|w| {
            if w[0] < w[1] {
                count += 1;
                true
            } else {
                false
            }
        })
        .collect();

    println!("{:?}", count);
}
