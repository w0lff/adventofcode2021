use std::path::PathBuf;

use clap::Parser;

mod part1;
mod part2;

#[derive(Parser)]
struct Opts {
    #[clap(default_value = "1")]
    part: u8,
    #[clap(default_value = "input.txt")]
    input: PathBuf,
}

fn main() {
    let opts = Opts::parse();
    match opts.part {
        1 => part1::run(opts.input),
        2 => part2::run(opts.input),
        _ => {
            eprintln!("Not a valid part");
        }
    }
}
