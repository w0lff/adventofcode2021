use crossbeam_channel::{unbounded, Receiver, Sender};
use std::{
    fs::File,
    io::{BufRead, BufReader},
    path::PathBuf,
};

pub fn run(input: PathBuf) {
    println!("{:#?}", input);
    let file = File::open(input).unwrap();
    let reader = BufReader::new(&file);
    let mut list: Vec<u32> = Vec::new();
    let mut longest_line = 0usize;

    // push parsed numbers into Vec
    for line in reader.lines() {
        let line = line.unwrap();
        let line_len = line.len();
        if line_len > longest_line {
            longest_line = line_len;
        }

        // line.parse::<u32>() does not work here because numbers are in binary
        // parse would read them as decimals
        let num = u32::from_str_radix(&line, 2).unwrap();
        list.push(num);

        // println!("{:0len$b}", num, len = line_len);
    }
    println!("longest line len: {}", longest_line);
    // msb - most significant bit
    let msb: u32 = (longest_line - 1).try_into().unwrap();
    println!("msb: {}", msb);

    let (g, e) = multi_threaded(list, msb);

    println!("gamma:   {:0len$b}", g, len = longest_line);
    println!("epsilon: {:0len$b}", e, len = longest_line);

    // neo layout goes brrr
    println!("ε * γ: {}", e * g);
}

// msb - most significant bit
fn multi_threaded(list: Vec<u32>, msb: u32) -> (u32, u32) {
    let (tx, rx): (Sender<Option<u32>>, Receiver<Option<u32>>) = unbounded();

    // start for every bit position a thread
    // e.g. msb == 3:
    // 0b001 # thread 0
    // 0b010 # thread 1
    // 0b100 # thread 2
    for bit in 0..=msb {
        let th_tx = tx.clone();
        let th_list = list.clone();
        let _jh = std::thread::spawn(move || inner(th_tx, th_list, bit));
    }

    let mut gamma = 0;
    let mut incoming_count = 0u32;
    // when a thread is done it send it's result here and gamma is calculated
    while incoming_count < msb + 1 {
        if let Ok(msg) = rx.try_recv() {
            incoming_count += 1;
            //println!("msg: {:?}", &msg);

            if let Some(n) = msg {
                gamma |= n;
            }
        }
    }

    // calculates epsilon from gamma
    let epsilon = !gamma & (2u32.pow(msb + 1) - 1);

    (gamma, epsilon)
}

// goes through every element and counts occurences of the specified `bit`
fn inner(tx: Sender<Option<u32>>, list: Vec<u32>, bit: u32) {
    let exp2: u32 = 2u32.pow(bit);

    let mut count = 0;
    list.iter().for_each(|n| {
        if (n & exp2) == exp2 {
            count += 1;
        }
    });

    // send the bit that has the most occurences;
    if count > list.len() / 2 {
        tx.send(Some(exp2)).unwrap();
    } else {
        tx.send(None).unwrap();
    }
}
