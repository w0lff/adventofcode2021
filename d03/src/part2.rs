use std::{
    fs::File,
    io::{BufRead, BufReader},
    path::PathBuf,
};

pub fn run(input: PathBuf) {
    println!("{:#?}", input);
    let file = File::open(input).unwrap();
    let reader = BufReader::new(&file);
    let mut list: Vec<u32> = Vec::new();
    let mut longest_line = 0usize;

    // push parsed numbers into Vec
    for line in reader.lines() {
        let line = line.unwrap();
        let line_len = line.len();
        if line_len > longest_line {
            longest_line = line_len;
        }

        // line.parse::<u32>() does not work here because numbers are in binary
        // parse would read them as decimals
        let num = u32::from_str_radix(&line, 2).unwrap();
        list.push(num);

        // println!("{:0len$b}", num, len = line_len);
    }
    println!("longest line len: {}", longest_line);
    // msb - most significant bit
    let msb: u32 = (longest_line - 1).try_into().unwrap();
    println!("msb: {}", msb);

    let (g, e): (u32, u32);

    g = filter_oxygen(list.clone(), msb);
    e = filter_co2(list, msb);

    println!("γ: {:0len$b}", g, len = longest_line);
    println!("ε: {:0len$b}", e, len = longest_line);

    println!("γ * ε: {}", g * e);
}

fn filter_oxygen(list: Vec<u32>, msb: u32) -> u32 {
    let mut list = list;
    for bit in (0..=msb).rev() {
        let bitmask: u32 = 2u32.pow(bit);
        let list_len = list.len();
        if list_len == 1 {
            break;
        }
        let mut count = 0usize;

        list.iter().for_each(|x| {
            if (*x & bitmask) == bitmask {
                count += 1;
            }
        });

        let zeros = list_len - count;
        let ones = count;

        list = list
            .iter()
            .filter_map(|x| {
                if ones >= zeros {
                    if (*x & bitmask) == bitmask {
                        return Some(*x);
                    }
                } else if (*x & bitmask) == 0 {
                    return Some(*x);
                }

                None
            })
            .collect();
    }
    list[0]
}

fn filter_co2(list: Vec<u32>, msb: u32) -> u32 {
    let mut list = list;
    for bit in (0..=msb).rev() {
        let bitmask: u32 = 2u32.pow(bit);
        let list_len = list.len();
        if list_len == 1 {
            break;
        }
        let mut count = 0usize;

        list.iter().for_each(|x| {
            if (*x & bitmask) == bitmask {
                count += 1;
            }
        });

        let zeros = list_len - count;
        let ones = count;

        list = list
            .iter()
            .filter_map(|x| {
                if (*x & bitmask) == bitmask {
                    if ones < zeros {
                        return Some(*x);
                    }
                } else if zeros <= ones {
                    return Some(*x);
                }

                None
            })
            .collect();
    }
    list[0]
}
