use std::path::PathBuf;

use clap::Parser;

#[derive(Parser)]
struct Opts {
    #[clap(default_value = "1")]
    part: u8,
    #[clap(default_value = "input.txt")]
    input: PathBuf,
}

fn main() {
    let opts = Opts::parse();
    match opts.part {
        1 => part1(opts.input),
        2 => part2(opts.input),
        _ => {
            eprintln!("Not a valid part");
        }
    }
}

fn part1(input: PathBuf) {
    println!("{:#?}", input);
}

fn part2(input: PathBuf) {
    println!("{:#?}", input);
}
